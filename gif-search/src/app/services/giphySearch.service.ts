import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment'

@Injectable({
  providedIn: 'root'
})

export class GiphySearch {
    constructor(private httpClient: HttpClient) { }

    getGifs(searchQuery){
      return this.httpClient.get('//api.giphy.com/v1/gifs/search?api_key='+environment.giphyKey+'&q='+searchQuery+'&limit=26&offset=0&rating=g&lang=en')
    }

    getCategoryList(){
      return this.httpClient.get('//api.giphy.com/v1/gifs/categories?api_key='+environment.giphyKey)
    }

    getTrendingGifsList(){
      return this.httpClient.get('//api.giphy.com/v1/gifs/trending?api_key='+environment.giphyKey)
    }

}