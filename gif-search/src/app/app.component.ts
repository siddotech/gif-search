import { Component, OnInit } from '@angular/core';
import { GiphySearch } from './services/giphySearch.service';
import { FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  searchForm:FormGroup
  title = 'gif-search';
  searchResult:any = [];
  randomPicked:any [];
  imageContainer:any = [];
  verticalStyle:boolean = false;
  horizontalStyle:boolean = false;
  categoryList:any = [];
  
  constructor(
    private formBuilder:FormBuilder,
    private giphySearch: GiphySearch
  ) { }

  ngOnInit() {
    this.searchForm = this.formBuilder.group({
      searchText:[''],
      selectTags:['']
    });
    this.getTrendingGifs();
    this.getGifsCategory();
  }


  searchBox(){
    this.searchForm.controls['selectTags'].setValue('All');
  }

// Search Api Consumed

  searchGif(){
    if(this.searchForm.value.searchText == "") {
      this.getTrendingGifs();
    }else {
      this.giphySearch.getGifs(this.searchForm.value.searchText).subscribe((giphyResponse: any) => {
        if(giphyResponse.meta.status == 200) {
          this.searchResult = giphyResponse.data;
        }else {
          alert('Some Server Error')
        }
      },
      error => {
        alert('The Error'+ error);
      }); 
    }
  }

// Search Api Consumed ends here


//Category Dropdown 

getGifsCategory(){
  this.giphySearch.getCategoryList().subscribe((getCatgoryList:any) => {
    this.categoryList = getCatgoryList.data;
  }) 
}

//Category Dropdown ends here

// Get Trending Api Consumed

  getTrendingGifs(){
    this.giphySearch.getTrendingGifsList().subscribe((getTrendingList: any) => {
      this.searchResult = getTrendingList.data;
    });
  }

// Get Trending Api Consumed ends here


//Search Categorywise Api consumed

  setGifsTag(){
    this.searchForm.controls['searchText'].setValue('');
    if(this.searchForm.value.selectTags == "All") {
      this.getTrendingGifs();
    }else {
      this.giphySearch.getGifs(this.searchForm.value.selectTags).subscribe((giphyResponse: any) => {
        if(giphyResponse.meta.status == 200) {
          this.searchResult = giphyResponse.data;
        }else {
          alert('Some Server Error')
        }
      },
      error => {
        alert('The Error'+ error);
      }); 
    }
  }

//Search Categorywise Api consumed ends here
 
}

